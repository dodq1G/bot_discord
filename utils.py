# utils.py
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.common.exceptions import TimeoutException
import discord
from discord import Button, ButtonStyle

def setup_webdriver():
    service = Service(ChromeDriverManager().install())
    options = webdriver.ChromeOptions()
    options.add_argument('--disable-gpu')
    options.add_argument('--no-sandbox')
    options.add_argument('--headless')
    options.add_argument("user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3")
    return webdriver.Chrome(service=service, options=options)

def get_car_data(url):
    driver = setup_webdriver()
    car_data = []

    try:
        driver.get(url)
        WebDriverWait(driver, 10).until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, 'a.vehicle-data')))
        
        vehicles = driver.find_elements(By.CSS_SELECTOR, 'a.vehicle-data')

        for vehicle in vehicles:
            name = vehicle.find_element(By.CSS_SELECTOR, 'h3.vehicle-title').text if vehicle.find_elements(By.CSS_SELECTOR, 'h3.vehicle-title') else 'Nom indisponible'
            price = vehicle.find_element(By.CSS_SELECTOR, 'p.seller-currency').text if vehicle.find_elements(By.CSS_SELECTOR, 'p.seller-currency') else 'Prix indisponible'
            car_url = vehicle.get_attribute('href')
            car_id = vehicle.get_attribute('data-vehicle-id')

            embed = discord.Embed(title="Voiture disponible", color=0x00ff00)
            embed.add_field(name="Nom du véhicule", value=name, inline=False)
            embed.add_field(name="Prix", value=price, inline=False)

            car_data.append((embed, car_id, car_url))

    except TimeoutException:
        embed = discord.Embed(title="Erreur", description="Le chargement de la page a pris trop de temps.", color=0xff0000)
        car_data.append((embed, None, None))
    except Exception as e:
        embed = discord.Embed(title="Erreur", description=f"Erreur lors de la récupération des données du site: {e}", color=0xff0000)
        car_data.append((embed, None, None))
    finally:
        driver.quit()

    return car_data

def get_car_details(url):
    driver = setup_webdriver()

    try:
        driver.get(url)
        WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'div.vehicle-details-main')))

        model_name = driver.find_element(By.CSS_SELECTOR, 'h1.g-col-8').text if driver.find_elements(By.CSS_SELECTOR, 'h1.g-col-8') else 'Non spécifié'
        model_tech = driver.find_element(By.CSS_SELECTOR, 'div.vip-details-block').text if driver.find_elements(By.CSS_SELECTOR, 'div.vip-details-block') else 'Non spécifié'

        lines = model_tech.split('\n')

        for i in range(1, len(lines), 2):
            lines[i] = "**" + lines[i] + "**"
        
        lines.pop(0)

        model_tech = '\n'.join(lines[:20])
        embed = discord.Embed(title=model_name, color=0x00ff00)  
        embed.add_field(name="Technologies et caractéristiques", value=model_tech, inline=False)
        
        return embed

    except TimeoutException:
        return discord.Embed(title="Erreur", description="Le chargement de la page a pris trop de temps.", color=0xff0000)
    except Exception as e:
        return discord.Embed(title="Erreur", description=f"Erreur lors de la récupération des détails de la voiture: {e}", color=0xff0000)
    finally:
        driver.quit()

    return car_details


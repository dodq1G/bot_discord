# bot.py
import discord
from request import fetch_car_data, fetch_car_details

class MyButton(discord.ui.Button):
    def __init__(self, car_id, car_url, **kwargs):
        super().__init__(**kwargs)
        self.car_id = car_id
        self.car_url = car_url

    async def callback(self, interaction: discord.Interaction):
        await interaction.response.send_message("Chargement des détails de la voiture...", ephemeral=True)
        car_details = fetch_car_details(self.car_url)
        await interaction.edit_original_response(content="", embed=car_details)


class MyView(discord.ui.View):
    def __init__(self, car_id, car_url):
        super().__init__(timeout=None)
        button = MyButton(style=discord.ButtonStyle.primary, label="Détails", car_id=car_id, car_url=car_url)
        self.add_item(button)

class MyClient(discord.Client):
    async def on_ready(self):
        print(f'Logged on as {self.user}!')

    async def on_message(self, message):
        if message.author == self.user:
            return

        if message.content.startswith('!hello'):
            await message.channel.send('Hello!')

        elif message.content.startswith('!valid'):
            await message.channel.send('This bot is valid.')

        elif message.content.startswith('!help'):
            help_text = ("Commandes disponibles :\n"
                         "!hello - Saluer le bot\n"
                         "!valid - Tester si le bot est valide\n"
                         "!help - Afficher ce message d'aide\n"
                         "!car [URL de la page] - Afficher les modèles du site automobile.fr")
            await message.channel.send(help_text)

        elif message.content.startswith('!car'):
            url = message.content.split(' ')[1]
            car_data = fetch_car_data(url)
            for embed, car_id, car_url in car_data:
                view = MyView(car_id, car_url)
                await message.channel.send(embed=embed, view=view)

intents = discord.Intents.default()
intents.messages = True
intents.message_content = True

client = MyClient(intents = intents)

token = "mytoken"
client.run(token)

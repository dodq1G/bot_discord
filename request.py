# request.py
from utils import get_car_data, get_car_details

def fetch_car_data(url):
    return get_car_data(url)

def fetch_car_details(url):
    return get_car_details(url)
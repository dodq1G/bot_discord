# Bot Discord pour Automobile.fr

## Introduction
Ce bot Discord permet aux utilisateurs de récupérer des informations sur les voitures disponibles sur automobile.fr. Il offre des fonctionnalités telles que l'affichage de détails sur les modèles de voitures, y compris les prix et les caractéristiques techniques.

## Installation

### Prérequis
- Python 3.8 ou plus récent.
- Bibliothèques Python : `discord.py`, `selenium`, `webdriver-manager`.

### Étapes d'installation
1. Clonez le dépôt ou téléchargez les fichiers du bot.
2. Installez les dépendances Python en exécutant `pip install -r requirements.txt`.
3. Créez un bot sur le [portail des développeurs Discord](https://discord.com/developers/applications) et obtenez un token.
4. Remplacez `"mytoken"` dans `bot.py` par votre token de bot Discord.

## Utilisation
Lancez le bot en exécutant `python bot.py`. Le bot se connectera à votre serveur Discord.

## Commandes disponibles
- `!hello` : Saluer le bot.
- `!valid` : Tester si le bot est valide.
- `!help` : Afficher un message d'aide avec la liste des commandes.
- `!car [URL de la page]` : Afficher les informations des modèles de voitures disponibles à l'URL spécifiée.

## Fonctionnalités
- Utilise Selenium pour scraper les données des voitures sur automobile.fr.
- Affiche les informations dans des embeds Discord avec des boutons pour des détails supplémentaires.
- Gère les exceptions et les timeouts lors du chargement des pages web.

## Note
La création d'un bot Discord représente une excellente méthode pour se familiariser avec le développement en Python. J'ai rencontré certaines difficultés lors de l'intégration des boutons et j'ai dû recourir à Selenium, car le contenu de la page était généré dynamiquement. Je pense qu'il serait possible de perfectionner davantage mon bot, par exemple, en lui permettant de récupérer les informations sur les véhicules les moins chers.
